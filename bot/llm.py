import asyncio

from groq import Groq

from . import constants


async def generate_response(message: str, prompt="You are a helpful AI assistant") -> str:
    client = Groq(api_key=constants.GROQ_KEY)

    chat_completion = client.chat.completions.create(
        messages=[
            {
                "role": "system",
                "content": prompt
            },
            {
                "role": "user",
                "content": message,
            }
        ],
        model="llama3-70b-8192",
        # this is already really fast, but you can use the 8b model for almost instant responses
        # https://console.groq.com/docs/models
    )

    return chat_completion.choices[0].message.content
