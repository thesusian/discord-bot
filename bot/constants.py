import os
import pathlib

from typing import NamedTuple

ENVIRONMENT = os.getenv("ENVIRONMENT")
if ENVIRONMENT is None:
    from dotenv import load_dotenv

    load_dotenv(dotenv_path=f"{os.getcwd()}/.env")

# Environment vars
BOT_TOKEN = os.getenv("BOT_TOKEN")
GROQ_KEY = os.getenv("GROQ_KEY")

# Paths
EXTENSIONS = pathlib.Path("bot/exts/")

if TEST_GUILDS := os.getenv("TEST_GUILDS"):
    TEST_GUILDS = [int(x) for x in TEST_GUILDS.split(",")]


class Channels(NamedTuple):
    log = int(os.getenv("CHANNEL_LOG", 1235899811618750464))
    dev_log = int(os.getenv("CHANNEL_LOG", 1235899811618750464))


class Roles(NamedTuple):
    moderator = int(os.getenv("ROLE_MODERATOR", 1157827751596785745))
    administrator = int(os.getenv("ROLE_ADMINISTRATOR", 1157827725613072486))
