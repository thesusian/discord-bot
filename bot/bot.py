import asyncio
import os
from datetime import datetime

from aiohttp import ClientSession
from disnake import AllowedMentions, Embed, Intents
from disnake.ext import commands

from . import constants


class Bot(commands.InteractionBot):
    """The core of the bot."""

    def __init__(self) -> None:
        intents = Intents.default()
        intents.members = True
        intents.presences = True

        self.http_session = ClientSession()

        test_guilds = None
        if constants.TEST_GUILDS:
            test_guilds = constants.TEST_GUILDS

        super().__init__(
            intents=intents,
            test_guilds=test_guilds,
            allowed_mentions=AllowedMentions(
                everyone=None,
                users=True,
                replied_user=True,
            ),
            reload=True,
        )

        self.initiated = False

    def load_extensions(self) -> None:
        """Load all extensions in the exts/ folder."""
        for extension in constants.EXTENSIONS.glob("*/*.py"):
            if extension.name.startswith("_"):
                continue  # Ignore files starting with _
            ext_path = str(extension).replace(os.sep, ".")[:-3]  # Truncate .py
            self.load_extension(ext_path)
            print(f"Extension loaded: {ext_path}")

    def run(self) -> None:
        """Run the bot with the token present in the .env."""
        if constants.BOT_TOKEN is None:
            raise EnvironmentError(
                "Token value is None. Make sure you have configured the BOT_TOKEN field in .env"
            )

        super().run(constants.BOT_TOKEN)

    async def on_ready(self) -> None:
        """Add extensions once the bot is connected to Discord and is ready."""
        if not self.initiated:
            await asyncio.sleep(2)
            self.load_extensions()
            self.initiated = True
        print("The bot is online!")

    async def startup_alert(self) -> None:
        """Announce bot's presence to the devlog channel."""
        embed = Embed(
            title="Bot Startup",
            description="The bot is back online!",
            timestamp=datetime.now(),
        )
        await self.get_channel(constants.Channels.dev_log).send(embed=embed)

    async def close(self) -> None:
        """Close the bot gracefully."""
        await super().close()
