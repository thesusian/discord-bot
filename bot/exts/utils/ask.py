from disnake import ApplicationCommandInteraction
from disnake.ext import commands

from ...bot import Bot
from ...llm import generate_response


class Ask(commands.Cog):
    """Return the answer to the users question using Groq"""

    def __init__(self, bot: commands.InteractionBot):
        self.bot = bot

    @commands.slash_command()
    async def ask(self, itr: ApplicationCommandInteraction, message: str) -> None:
        if not itr.author.guild_permissions.administrator:
            return

        await itr.response.defer()
        await itr.channel.send(await generate_response(message=message))
        await itr.delete_original_response()


def setup(bot: Bot) -> None:
    """Load the Ask cog."""
    bot.add_cog(Ask(bot))
