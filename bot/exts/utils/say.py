from disnake import ApplicationCommandInteraction
from disnake.ext import commands

from ...bot import Bot


class Say(commands.Cog):
    """Return a message as specified by the user."""

    def __init__(self, bot: commands.InteractionBot):
        self.bot = bot

    @commands.slash_command()
    async def say(self, itr: ApplicationCommandInteraction, message: str) -> None:
        if not itr.author.guild_permissions.administrator:
            return

        await itr.channel.send(message)

        await itr.response.send_message("Done.", ephemeral=True)


def setup(bot: Bot) -> None:
    """Load the Say cog."""
    bot.add_cog(Say(bot))
